﻿using Library.Shared;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserLibrary.Services
{
	public class BookingServices  // : IAsyncDisposable -> TODO phần DispoAsync()
	{
		HubConnection _hubConnection;
		private readonly NavigationManager _navigationManager;
		// public event Action<BookingRequestItem> OnReceivedItem;
		//public event Action<string, BookingRequestItem.StateType> OnReceivedState;
		public event Action<BookingRequestItem> OnReceivedState;
		public event Action<int> OnUpdateDriverCount;

		public BookingServices(NavigationManager navigationManager)
		{
			_navigationManager = navigationManager;
		}

		public async Task StartConnection()
		{
            ServerURL sUrl = new ServerURL();
            if (_hubConnection is not null)
				return;
			_hubConnection = new HubConnectionBuilder()
				.WithUrl(_navigationManager.ToAbsoluteUri(sUrl.sURL + "/bookingrequesthub"))
				.Build();
			// _hubConnection.On<BookingRequestItem>("ReceiveItem", (item) => OnReceivedItem?.Invoke(item));
			_hubConnection.On<int>("UpdateDriverCount", (count) => OnUpdateDriverCount?.Invoke(count));
			//_hubConnection.On<string, BookingRequestItem.StateType>("ReceiveState", (bookingId, newState) => OnReceivedState?.Invoke(bookingId, newState));
			_hubConnection.On<BookingRequestItem>("ReceiveState", (item) => OnReceivedState?.Invoke(item));

			await _hubConnection.StartAsync();
			await JoinGroup();
		}

		public async Task JoinGroup()
		{
			if (_hubConnection is null) return;
			await _hubConnection.SendAsync("AddToGroup", "users");
		}

		public async Task LeaveGroup()
		{
			if (_hubConnection is null) return;
			await _hubConnection.SendAsync("RemoveFromGroup", "users");
		}

		public async Task JoinCurrentTripProcessGroup(string bookingId)
		{
			if (_hubConnection is null) return;
			await _hubConnection.SendAsync("AddToCurrentTripProcessGroup", bookingId);
		}

		public async Task LeaveCurrentTripProcessGroup(string bookingId)
		{
			if (_hubConnection is null) return;
			await _hubConnection.SendAsync("RemoveFromCurrentTripProcessGroup", bookingId);
		}

		public async Task SendBookingRequestToDriver(BookingRequestItem item)
		{
			if (_hubConnection is null) return;
			await _hubConnection.SendAsync("SendBookingRequestToDriver", item);
		}

		// ngắt kết nối khi rời khỏi trang (chuyển trang khác) là không cần thiết
		//TODO : điều này nên được gọi khi tắt chương trình
		public async ValueTask DisposeAsync()
		{
			if (_hubConnection is not null)
			{
				await LeaveGroup();
				await _hubConnection.DisposeAsync();
			}
		}
	}
}
