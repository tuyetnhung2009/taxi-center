﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Routing;
using Microsoft.AspNetCore.SignalR.Client;
using Library.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserLibrary.Services
{
	public class GpsTrackingServices // : IAsyncDisposable -> TODO phần DispoAsync()
	{
		HubConnection _hubConnection;
		private readonly NavigationManager _navigationManager;
		public event Action<GpsItem> OnReceivedItem;
		public event Action<int> OnDriverCountUpdated;

		public GpsTrackingServices(NavigationManager navigationManager)
		{
			_navigationManager = navigationManager;
		}

		public async Task StartConnection()
		{
            ServerURL sUrl = new ServerURL();
            if (_hubConnection is not null)
				return;
			_hubConnection = new HubConnectionBuilder()
				.WithUrl(_navigationManager.ToAbsoluteUri(sUrl.sURL + "/gpstrackinghub"))
				.Build();
			_hubConnection.On<GpsItem>("ReceiveItem", (item) => OnReceivedItem?.Invoke(item));
			_hubConnection.On<int>("UpdateDriverCount", (count) => OnDriverCountUpdated?.Invoke(count));

			await _hubConnection.StartAsync();
			await JoinGroup();
		}

		public async Task JoinGroup()
		{
			if (_hubConnection is null) return;
			await _hubConnection.SendAsync("AddToGroup", "users");
		}

		public async Task LeaveGroup()
		{
			if (_hubConnection is null) return;
			await _hubConnection.SendAsync("RemoveFromGroup", "users");
		}

		// ngắt kết nối khi rời khỏi trang (chuyển trang khác) là không cần thiết
		//TODO : điều này nên được gọi khi tắt chương trình
		public async ValueTask DisposeAsync()
		{
			if (_hubConnection is not null)
			{
				await LeaveGroup();
				await _hubConnection.DisposeAsync();
			}
		}
	}
}