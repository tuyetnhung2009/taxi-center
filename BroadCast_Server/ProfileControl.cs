﻿using MongoDB.Bson;
using MongoDB.Driver;
using System.Security.Cryptography;
using System.Text;

namespace BroadCast_Server
{
    public class ProfileControl : IProfileFilter, IProfileControl
    {
        private readonly IMongoCollection<BsonDocument> _openSessions;
        private readonly IMongoCollection<UserProfile> _userProfiles;
        private readonly IMongoCollection<DriverProfile> _driverProfiles;
        private readonly IMongoCollection<OperatorProfile> _operatorProfiles;

        public ProfileControl(IDatabaseSetting settings, IMongoClient mongo)
        {
            var database = mongo.GetDatabase(settings.DatabaseName);
            _userProfiles = database.GetCollection<UserProfile>(settings.UserProfiles);
            _driverProfiles = database.GetCollection<DriverProfile>(settings.DriverProfiles);
            _operatorProfiles = database.GetCollection<OperatorProfile>(settings.OperatorProfiles);
            _openSessions = database.GetCollection<BsonDocument>(settings.OpenSessions);
        }

        public Profile AddProfile(Profile newProfile)
        {
            switch (newProfile.Role)
            {
                case ProfileType.User:
                    {
                        Console.WriteLine("Adding new User");
                        var castedProfile = (UserProfile)newProfile;
                        _userProfiles.InsertOne(castedProfile);
                        break;
                    }
                case ProfileType.Driver:
                    {
                        Console.WriteLine("Adding new Driver");
                        var castedProfile = (DriverProfile)newProfile;
                        _driverProfiles.InsertOne(castedProfile);
                        break;
                    }
                case ProfileType.Operator:
                    {
                        Console.WriteLine("Adding new Operator");
                        var castedProfile = (OperatorProfile)newProfile;
                        _operatorProfiles.InsertOne(castedProfile);
                        break;
                    }
            }

            return newProfile;
        }

        public List<Profile> FilterByEmail(List<Profile> ProfileList, string FindingEmail)
        {
            throw new NotImplementedException();
        }

        public List<Profile> FilterByName(List<Profile> ProfileList, string FindingName)
        {
            throw new NotImplementedException();
        }

        public List<Profile> FilterByPhone(List<Profile> ProfileList, string FindingPhone)
        {
            throw new NotImplementedException();
        }

        public List<Profile> FilterByRole(List<Profile> ProfileList, string FindingRole)
        {
            throw new NotImplementedException();
        }

        public Profile GetProfileByID(string _id)
        {
            Profile profile;
            switch (_id[0])
            {
                case 'U':
                    {
                        profile = _userProfiles.Find(element => element.ObjectDataId == _id).FirstOrDefault();
                        break;
                    }
                case 'D':
                    {
                        profile = _driverProfiles.Find(element => element.ObjectDataId == _id).FirstOrDefault();
                        break;
                    }
                case 'O':
                    {
                        profile = _operatorProfiles.Find(element => element.ObjectDataId == _id).FirstOrDefault();
                        break;
                    }
                default:
                    {
                        throw new Exception("Unexpected ID: " + _id[0]);
                    }
            }

            return profile;
        }

        public List<Profile> GetProfiles(ProfileType mode)
        {
            switch (mode)
            {
                case ProfileType.Operator:
                    {
                        var userList = _userProfiles.Find(_ => true).ToList();
                        var driverList = _driverProfiles.Find(_ => true).ToList();
                        List<Profile> returnedProfiles = new List<Profile>();

                        foreach (var profile in userList)
                        {
                            returnedProfiles.Add((Profile)profile);
                        }
                        foreach (var profile in driverList)
                        {
                            returnedProfiles.Add((Profile)profile);
                        }

                        return returnedProfiles;
                    }
                case ProfileType.Admin:
                    {
                        var userList = _userProfiles.Find(_ => true).ToList();
                        var driverList = _driverProfiles.Find(_ => true).ToList();
                        var operatorList = _operatorProfiles.Find(_ => true).ToList();
                        List<Profile> returnedProfiles = new List<Profile>();

                        foreach (var profile in userList)
                        {
                            returnedProfiles.Add((Profile)profile);
                        }
                        foreach (var profile in driverList)
                        {
                            returnedProfiles.Add((Profile)profile);
                        }
                        foreach (var profile in operatorList)
                        {
                            returnedProfiles.Add((Profile)profile);
                        }

                        return returnedProfiles;
                    }
                default:
                    {
                        return new List<Profile>();
                    }
            }
        }

        public void RemoveProfile(string _id)
        {
            switch (_id[0])
            {
                case 'U':
                    {
                        _userProfiles.DeleteOne(profile => profile.ObjectDataId == _id);
                        break;
                    }
                case 'D':
                    {
                        _driverProfiles.DeleteOne(profile => profile.ObjectDataId == _id);
                        break;
                    }
                case 'O':
                    {
                        _operatorProfiles.DeleteOne(profile => profile.ObjectDataId == _id);
                        break;
                    }
                default:
                    {
                        throw new Exception($"Unexpected ID: \'{_id[0]}\'");
                    }
            }
        }

        public void UpdateProfile(string _id, Profile updatedProfile)
        {
            switch (_id[0])
            {
                case 'U':
                    {
                        _userProfiles.ReplaceOne(profile => profile.ObjectDataId == _id, (UserProfile)updatedProfile);
                        break;
                    }
                case 'D':
                    {
                        _driverProfiles.ReplaceOne(profile => profile.ObjectDataId == _id, (DriverProfile)updatedProfile);
                        break;
                    }
                case 'O':
                    {
                        _operatorProfiles.ReplaceOne(profile => profile.ObjectDataId == _id, (OperatorProfile)updatedProfile);
                        break;
                    }
                default:
                    {
                        throw new Exception($"Unexpected ID: \'{_id[0]}\'");
                    }
            }
        }

        public void NewSession(string _hashsession)
        {
            throw new NotImplementedException();
        }

        public void SaveToken(SessionToken _token, Profile profile)
        {
            using (var sha256 = SHA256.Create())
            {
                // Combine token with profile password
                var tokenByte = Encoding.UTF8.GetBytes(_token.ToJson());  // Better way to hash?

                // Save hashed
                byte[] hashToken = sha256.ComputeHash(tokenByte);
                _openSessions.InsertOne(new BsonDocument().Add("Experation", new BsonDateTime(_token.Expiration.ToUniversalTime())).Add("HashID", new BsonBinaryData(hashToken)));
            }
        }

        public SessionToken? Authenticate(string _emailOrPhone, string _password, ProfileType _profileType)
        {
            Profile? profile = null;
            SessionToken? newToken = null;

            switch (_profileType)
            {
                case ProfileType.User:
                    {
                        profile = _userProfiles.Find(profile => (profile.Phone == _emailOrPhone || profile.Email == _emailOrPhone) && profile.Password == _password).FirstOrDefault();
                        break;
                    }
                case ProfileType.Driver:
                    {
                        profile = _driverProfiles.Find(profile => (profile.Phone == _emailOrPhone || profile.Email == _emailOrPhone) && profile.Password == _password).FirstOrDefault();
                        break;
                    }
                case ProfileType.Operator:
                    {
                        profile = _operatorProfiles.Find(profile => (profile.Phone == _emailOrPhone || profile.Email == _emailOrPhone) && profile.Password == _password).FirstOrDefault();
                        break;
                    }
                default:
                    {
                        break;
                    }
            }

            if (profile != null)
            {
                newToken = new SessionToken()
                {
                    ObjectId = profile.ObjectDataId,
                    Expiration = DateTime.Now.AddDays(7)
                };
                SaveToken(newToken, profile);
            }

            return newToken;
        }

        public bool CheckSession(byte[] _hashsession)
        {
            foreach (var session in _openSessions.Find(_ => true).ToList<BsonDocument>())
            {
                var savedByte = session["HashID"].AsByteArray;
                if (savedByte.SequenceEqual(_hashsession))
                {
                    return true;
                    }
            }
            return false;
        }
    }
}
