﻿namespace BroadCast_Server
{
    public class OperatorProfile : Profile
    {
        public OperatorProfile()
        {
            Role = ProfileType.Operator;
        }
    }
}
