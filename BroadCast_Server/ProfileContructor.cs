﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace BroadCast_Server
{
    public class ProfileContructor
    {
        public string ObjectDataId { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public string Phone { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        //public byte[] Password { get; set; } = new byte[0] { };
        public string Password { get; set; } = string.Empty;

        public Profile toProfile()
        {
            Profile profile;

            switch (ObjectDataId[0])
            {
                case 'U':
                    {
                        profile = new UserProfile();
                        break;
                    }
                case 'D':
                    {
                        profile = new DriverProfile();
                        break;
                    }
                case 'O':
                    {
                        profile = new OperatorProfile();
                        break;
                    }
                default:
                    {
                        throw new Exception($"Invalid Id: {ObjectDataId}");
                    }
            }

            profile.ObjectDataId = ObjectDataId;
            profile.Name = Name;
            profile.Phone = Phone;
            profile.Email = Email;
            profile.Password = Password;

            return profile;
        }
    }
}
