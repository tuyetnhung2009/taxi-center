﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

namespace BroadCast_Server
{
    public enum ProfileType
    {
        User,
        Driver,
        Operator,
        Admin
    }

    public abstract class Profile
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; } = string.Empty;

        [BsonElement("ObjectDataId")]
        public string ObjectDataId { get; set; } = string.Empty;

        [BsonElement("Name")]
        public string Name { get; set; } = string.Empty;

        [BsonElement("Phone")]
        public string Phone { get; set; } = string.Empty;

        [BsonElement("Role")]
        public ProfileType Role { get; set; } = ProfileType.User;

        [BsonElement("Email")]
        public string Email { get; set; } = string.Empty;

        [BsonElement("Password")]
        //public byte[] Password { get; set; } = new byte[0] { };
        public string Password { get; set; } = string.Empty;
    }
}
