﻿using Library.Shared;

namespace BroadCast_Server.Hubs
{
    public interface IHub
    {
        Task ReceiveItem(GpsItem item);
        Task ReceiveItem(BookingRequestItem item);
        Task UpdateUsersCount(int count);
        Task UpdateDriverCount(int count);
        Task UpdateCallCenterCount(int count);
        //Task ReceiveState(string bookingId, BookingRequestItem.StateType newState);
        Task ReceiveState(BookingRequestItem item);
    }
}
