﻿using Microsoft.AspNetCore.SignalR;
using Library.Shared;

namespace BroadCast_Server.Hubs
{
    public class GpsTrackingHub : Hub<IHub>
    {
        static int usersCount;
        static int driversCount;
        static int callCenterCount;

		public async Task AddToGroup(string groupName)
		{
			switch (groupName)
			{
				case "users":
					usersCount++;
					await Groups.AddToGroupAsync(Context.ConnectionId, "users");
					await Clients.All.UpdateUsersCount(usersCount);
					break;
				case "drivers":
					driversCount++;
					await Groups.AddToGroupAsync(Context.ConnectionId, "drivers");
					await Clients.All.UpdateDriverCount(driversCount);
					break;
				case "callcenter":
					callCenterCount++;
					await Groups.AddToGroupAsync(Context.ConnectionId, "callcenter");
					await Clients.All.UpdateCallCenterCount(callCenterCount);
					break;
			}
		}

		public async Task RemoveFromGroup(string groupName)
		{
			switch (groupName)
			{
				case "users":
					usersCount--;
					await Groups.RemoveFromGroupAsync(Context.ConnectionId, "users");
					await Clients.All.UpdateUsersCount(usersCount);
					break;
				case "drivers":
					driversCount--;
					await Groups.RemoveFromGroupAsync(Context.ConnectionId, "drivers");
					await Clients.All.UpdateDriverCount(driversCount);
					break;
				case "callcenter":
					callCenterCount--;
					await Groups.RemoveFromGroupAsync(Context.ConnectionId, "callcenter");
					await Clients.All.UpdateCallCenterCount(callCenterCount);
					break;
			}
		}

		public async Task SendGpsToUserAndCallCenter(GpsItem item)
        {
            await Clients.Group("users").ReceiveItem(item);
            await Clients.Group("callcenter").ReceiveItem(item);
        }

        public override async Task OnConnectedAsync()
        {
            await base.OnConnectedAsync();
			await Clients.All.UpdateUsersCount(usersCount);
			await Clients.All.UpdateDriverCount(driversCount);
			await Clients.All.UpdateCallCenterCount(callCenterCount);
        }

        public override async Task OnDisconnectedAsync(Exception? exception)
        {
            await base.OnDisconnectedAsync(exception);
        }
    }
}
