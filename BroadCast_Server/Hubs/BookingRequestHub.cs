﻿using Microsoft.AspNetCore.SignalR;
using Library.Shared;

namespace BroadCast_Server.Hubs
{
    public class BookingRequestHub : Hub<IHub>
    {
        static int usersCount;
        static int driversCount;
        static int callCenterCount;
        public override async Task OnConnectedAsync()
        {
            await base.OnConnectedAsync();
            await Clients.All.UpdateUsersCount(usersCount);
            await Clients.All.UpdateDriverCount(driversCount);
            await Clients.All.UpdateCallCenterCount(callCenterCount);
        }

        public async Task AddToGroup(string groupName)
        {
            switch (groupName)
            {
                case "users":
                    usersCount++;
                    await Groups.AddToGroupAsync(Context.ConnectionId, "users");
                    await Clients.All.UpdateUsersCount(usersCount);
                    break;
                case "drivers":
                    driversCount++;
                    await Groups.AddToGroupAsync(Context.ConnectionId, "drivers");
                    await Clients.All.UpdateDriverCount(driversCount);
                    break;
                case "callcenter":
                    callCenterCount++;
                    await Groups.AddToGroupAsync(Context.ConnectionId, "callcenter");
                    await Clients.All.UpdateCallCenterCount(callCenterCount);
                    break;
            }
        }

        public async Task RemoveFromGroup(string groupName)
        {
            switch (groupName)
            {
                case "users":
                    usersCount--;
                    await Groups.RemoveFromGroupAsync(Context.ConnectionId, "users");
                    await Clients.All.UpdateUsersCount(usersCount);
                    break;
                case "drivers":
                    driversCount--;
                    await Groups.RemoveFromGroupAsync(Context.ConnectionId, "drivers");
                    await Clients.All.UpdateDriverCount(driversCount);
                    break;
                case "callcenter":
                    callCenterCount--;
                    await Groups.RemoveFromGroupAsync(Context.ConnectionId, "callcenter");
                    await Clients.All.UpdateCallCenterCount(callCenterCount);
                    break;
            }
        }

        public async Task AddToCurrentTripProcessGroup(string idBookingRequest)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, idBookingRequest);
        }

        public async Task RemoveFromCurrentTripProcessGroup(string idBookingRequest)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, idBookingRequest);
        }

        public async Task SendBookingRequestToDriver(BookingRequestItem item)
        {
            await Clients.Group("drivers").ReceiveItem(item);
        }

        //public async Task UpdateBookingState(string bookingId, BookingRequestItem.StateType newState)
        //{
        //    await Clients.Group(bookingId.ToString()).ReceiveState(bookingId, newState);
        //}
        public async Task UpdateBookingState(BookingRequestItem item)
        {
            if (item != null && item.BookingID != null)
                await Clients.Group(item.BookingID.ToString()).ReceiveState(item);
        }

        public async Task UpdateBookingStateToDriver(BookingRequestItem item)
        {
            await Clients.Group("drivers").ReceiveState(item);
        }


        public override Task OnDisconnectedAsync(Exception? exception)
        {
            return base.OnDisconnectedAsync(exception);
        }
    }
}
