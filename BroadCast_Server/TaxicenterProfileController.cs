﻿using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using System.Security.Cryptography;
using System.Text;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BroadCast_Server
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaxicenterProfileController : ControllerBase
    {
        private readonly IProfileControl profileControl;

        public TaxicenterProfileController(IProfileControl profileControl)
        {
            this.profileControl = profileControl;
        }

        // GET: api/<TaxicenterProfileControler>?mode=3
        [HttpGet("m={mode}")]
        public ActionResult<List<Profile>> Get(ProfileType mode)
        {
            return profileControl.GetProfiles(mode);      //TODO: add mode control
        }

        // GET api/<TaxicenterProfileControler>?profileId=U001A6
        [HttpGet("profileId={ObjectDataId}")]
        public ActionResult<Profile> Get(string ObjectDataId)
        {
            Profile profile = profileControl.GetProfileByID(ObjectDataId);           //TODO: add exception handling

            if (profile == null)
            {
                return NotFound($"Profile with ObjectDataId = {ObjectDataId} not found");
            }

            return profile;
        }

        // POST api/<TaxicenterProfileControler>
        [HttpPost]
        public ActionResult<Profile> Post([FromBody] ProfileContructor profile)
        {
            var newProfile = profile.toProfile();
            profileControl.AddProfile(newProfile);

            return CreatedAtAction(nameof(Get), new { ObjectDataId = newProfile.ObjectDataId }, newProfile);
        }

        // PUT api/<TaxicenterProfileControler>/5
        [HttpPut("profileId={ObjectDataId}")]
        public ActionResult Put(string ObjectDataId, [FromBody] Profile profile)
        {
            var existedProfile = profileControl.GetProfileByID(ObjectDataId);

            if (existedProfile == null)
            {
                return NotFound($"Profile with ObjectDataId = {ObjectDataId} not found");
            }

            profileControl.UpdateProfile(ObjectDataId, profile);

            return NoContent();
        }

        // DELETE api/<TaxicenterProfileControler>/5
        [HttpDelete("profileId={ObjectDataId}")]
        public ActionResult Delete(string ObjectDataId)
        {
            var existedProfile = profileControl.GetProfileByID(ObjectDataId);

            if (existedProfile == null)
            {
                return NotFound($"Profile with ObjectDataId = {ObjectDataId} not found");
            }

            profileControl.RemoveProfile(ObjectDataId);

            return Ok($"Profile with ObjectDataId = {ObjectDataId} deleted");
        }

        [HttpPost("test")]
        public void Test()
        {

        }

        [HttpGet("auth/u={username}&p={password}&t={type}")]
        public ActionResult<SessionToken> Authenticate(string username = "hao@", string password = "hao", ProfileType type = 0)
        {
            Console.WriteLine($"Authenticate attempt for profile {username} : {password} : {type}");

            var token = profileControl.Authenticate(username, password, type);

            if (token == null)
            {
                return NotFound($"Username or password is incorrect");
            }

            return token;
        }

        [HttpGet("auth/i={sessionID}&e={experation}")]
        public ActionResult<SessionToken> CheckSession(string sessionID = "U001", string experation = "2023-07-19T15:30:06.4933542+07:00")
        {
            Console.WriteLine($"Authenticate attempt for token {sessionID} : {experation}");
            var session = new SessionToken
            {
                Expiration = DateTime.Parse(experation).ToUniversalTime(),
                ObjectId = sessionID
            };

            using (var sha256 = SHA256.Create())
            {
                var hashToken = sha256.ComputeHash(Encoding.UTF8.GetBytes(session.ToJson()));
                if (profileControl.CheckSession(hashToken))
                {
                    return Ok("Session Found");
                }
            }

            return NotFound("Session not found");
        }
    }
}
