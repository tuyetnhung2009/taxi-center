﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace BroadCast_Server
{
    public class SessionToken
    {
        [BsonElement("ObjectId")]
        public string ObjectId { get; set; } = string.Empty;

        [BsonElement("Expiration")]
        public DateTime Expiration { get; set; } = DateTime.MinValue;
    }
}