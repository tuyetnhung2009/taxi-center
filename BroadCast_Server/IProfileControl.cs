﻿namespace BroadCast_Server
{
    public interface IProfileControl
    {
        public List<Profile> GetProfiles(ProfileType mode);
        public Profile GetProfileByID(string _id);
        public Profile AddProfile(Profile newProfile);
        public void RemoveProfile(string _id);
        public void UpdateProfile(string _id, Profile updatedProfile);
        public SessionToken? Authenticate(string _emailOrPhone, string _password, ProfileType _profileType);
        public void NewSession(string _hashsession);
        public bool CheckSession(byte[] _hashsession);
        public void SaveToken(SessionToken _token, Profile profile);
    }
}
