using BroadCast_Server;
using BroadCast_Server.Hubs;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// API Services
builder.Services.Configure<DatabaseSetting>(
                builder.Configuration.GetSection(nameof(DatabaseSetting)));

builder.Services.AddSingleton<IDatabaseSetting>(sp =>
    sp.GetRequiredService<IOptions<DatabaseSetting>>().Value);

builder.Services.AddSingleton<IMongoClient>(_ =>
    new MongoClient(builder.Configuration.GetValue<string>("DatabaseSetting:ConnectionString")));

builder.Services.AddScoped<IProfileControl, ProfileControl>();
builder.Services.AddScoped<IProfileFilter, ProfileControl>();

// SignalR Sevices
builder.Services.AddSignalR();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.MapHub<GpsTrackingHub>("/gpstrackinghub");
app.MapHub<BookingRequestHub>("/bookingrequesthub");

app.Run();
