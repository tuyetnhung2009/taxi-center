﻿namespace BroadCast_Server
{
    public class DatabaseSetting : IDatabaseSetting
    {
        public string DriverProfiles { get; set; } = String.Empty;
        public string OperatorProfiles { get; set; } = String.Empty;
        public string UserProfiles { get; set; } = String.Empty;
        public string ConnectionString { get; set; } = String.Empty;
        public string DatabaseName { get; set; } = String.Empty;
        public string OpenSessions { get; set; } = String.Empty;
    }
}
