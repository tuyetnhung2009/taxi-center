﻿namespace BroadCast_Server
{
    public interface IProfileFilter
    {
        public List<Profile> FilterByName(List<Profile> ProfileList, string FindingName);
        public List<Profile> FilterByEmail(List<Profile> ProfileList, string FindingEmail);
        public List<Profile> FilterByRole(List<Profile> ProfileList, string FindingRole);
        public List<Profile> FilterByPhone(List<Profile> ProfileList, string FindingPhone);
    }
}
