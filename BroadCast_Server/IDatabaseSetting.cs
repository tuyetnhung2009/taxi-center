﻿namespace BroadCast_Server
{
    public interface IDatabaseSetting
    {
        public string DriverProfiles { get; set; }
        public string OperatorProfiles { get; set; }
        public string UserProfiles { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
        public string OpenSessions { get; set; }
    }
}
