﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Shared
{
    public class Profile
    {
        public string ObjectDataId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public int Role { get; set; }
        public string Email { get; set; }

        //Constructor
        public Profile( Profile prf) 
        {
            if (prf != null)
            {
                ObjectDataId = prf.ObjectDataId;
                Name = prf.Name;
                Phone = prf.Phone;
                Role = prf.Role;
                Email = prf.Email;
            }
        }
    }
}
