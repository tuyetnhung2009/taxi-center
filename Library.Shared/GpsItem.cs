﻿namespace Library.Shared
{
    public class GpsItem
    {
        public Guid Id { get; set; }
		public string? phoneNumber { get; set; }
		public double? latitude { get; set; }
		public double? longitude { get; set; }
	}
}