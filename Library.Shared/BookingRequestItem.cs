﻿namespace Library.Shared
{
	public class BookingRequestItem
	{
		public enum StateType
		{
			Pending = 0,
			Accepted = 1,
			OnTrip = 2,
			Cancel = 3,
			Completed = 4
		}

		public string? BookingID { get; set; }
		public StateType state { get; set; } = 0; // 0 : pending || 1 : accepted || 2 : On trip || 2 : cancel || 3 : completed
		public string? phoneNumber { get; set; }
		public double? fromLatitude { get; set; }
		public double? fromLongitude { get; set; }
		public double? toLatitude { get; set; }
		public double? toLongitude { get; set; }
	}
}
