﻿using Library.Shared;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallCenterLibrary.Services
{
	public class GpsTrackingServices // : IAsyncDisposable -> TODO phần DispoAsync()
	{
		HubConnection _hubConnection;
		private readonly NavigationManager _navigationManager;
		public event Action<GpsItem> OnReceivedItem;
		public event Action<int> OnDriversCountUpdated;
		public event Action<int> OnUsersCountUpdate;

		public GpsTrackingServices(NavigationManager navigationManager)
		{
			_navigationManager = navigationManager;
		}

		public async Task StartConnection()
		{
            ServerURL sUrl = new ServerURL();
            if (_hubConnection is not null)
				return;
			_hubConnection = new HubConnectionBuilder()
				.WithUrl(_navigationManager.ToAbsoluteUri(sUrl.sURL + "/gpstrackinghub"))
				.Build();
			_hubConnection.On<GpsItem>("ReceiveItem", (item) => OnReceivedItem?.Invoke(item));
			_hubConnection.On<int>("UpdateDriversCount", (count) => OnDriversCountUpdated?.Invoke(count));
			_hubConnection.On<int>("UpdateUsersCount", (count) => OnUsersCountUpdate?.Invoke(count));

			await _hubConnection.StartAsync();
			await JoinGroup();
		}

		public async Task JoinGroup()
		{
			if (_hubConnection is null) return;
			await _hubConnection.SendAsync("AddToGroup", "callcenter");
		}

		public async Task LeaveGroup()
		{
			if (_hubConnection is null) return;
			await _hubConnection.SendAsync("RemoveFromGroup", "callcenter");
		}

		// ngắt kết nối khi rời khỏi trang (chuyển trang khác) là không cần thiết
		//TODO : điều này nên được gọi khi tắt chương trình
		public async ValueTask DisposeAsync()
		{
			if (_hubConnection is not null)
			{
				await LeaveGroup();
				await _hubConnection.DisposeAsync();
			}
		}
	}
}
