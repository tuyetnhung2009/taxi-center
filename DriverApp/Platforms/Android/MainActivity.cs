﻿using Android;
using Android.App;
using Android.Content.PM;
using Android.OS;
using AndroidX.Core.App;
using AndroidX.Core.Content;

namespace DriverApp
{
    [Activity(Theme = "@style/Maui.SplashTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode | ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize | ConfigChanges.Density)]
    public class MainActivity : MauiAppCompatActivity
    {
        private static readonly int RequestLocationPermission = 1;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // ...

            // Kiểm tra xem quyền đã được cấp hay chưa
            if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.AccessFineLocation) != Permission.Granted)
            {
                // Nếu quyền chưa được cấp, yêu cầu quyền
                ActivityCompat.RequestPermissions(this, new string[] { Manifest.Permission.AccessFineLocation }, RequestLocationPermission);
            }
        }

        // Xử lý kết quả yêu cầu quyền từ người dùng
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            if (requestCode == RequestLocationPermission)
            {
                if (grantResults.Length > 0 && grantResults[0] == Permission.Granted)
                {
                    // Quyền đã được cấp, bạn có thể sử dụng vị trí ở đây
                }
                else
                {
                    // Người dùng từ chối cấp quyền, xử lý theo cách phù hợp
                }
            }
            else
            {
                base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }

        // ...
    }
}