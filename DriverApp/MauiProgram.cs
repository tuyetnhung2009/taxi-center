﻿using DriverApp.Services;
using Microsoft.Extensions.Logging;

namespace DriverApp
{
    public static class MauiProgram
    {
        public static MauiApp CreateMauiApp()
        {
            var builder = MauiApp.CreateBuilder();
            builder
                .UseMauiApp<App>()
                .ConfigureFonts(fonts =>
                {
                    fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                });

            builder.Services.AddMauiBlazorWebView();
            builder.Services.AddScoped<GpsTrackingServices>();
            builder.Services.AddScoped<BookingServices>();
            builder.Services.AddSingleton<SharedRequestDataService>(provider => new SharedRequestDataService(null));
            builder.Services.AddSingleton<ProfileService>(provider => new ProfileService());

#if DEBUG
		builder.Services.AddBlazorWebViewDeveloperTools();
		builder.Logging.AddDebug();
#endif

            return builder.Build();
        }
    }
}