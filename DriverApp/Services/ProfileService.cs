﻿using Library.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;

namespace DriverApp.Services
{
    public class ProfileService
    {
        public Profile profile { get; set; } = new Profile(null);
        private HttpClient httpClient;
        private string sURL = new ServerURL().sURL;

        public ProfileService()
        {
            // Khởi tạo HttpClient
            httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(sURL); // Thay đổi địa chỉ URL của server của bạn
        }

        public async Task getProfileInf(string ObjectDataID)
        {
            try
            {
                // Gửi yêu cầu GET đến server và nhận phản hồi
                HttpResponseMessage response = await httpClient.GetAsync($"/api/TaxicenterProfile/profileId={ObjectDataID}"); // Thay đổi URL endpoint của bạn

                // Đảm bảo yêu cầu thành công (status code 200-299)
                response.EnsureSuccessStatusCode();

                //// Đọc nội dung phản hồi và lấy thông tin từ response
                string responseContent = await response.Content.ReadAsStringAsync();
                var jsonResponse = JObject.Parse(responseContent);

                profile.ObjectDataId = jsonResponse["objectDataId"].ToString();
                profile.Name = jsonResponse["name"].ToString();
                profile.Email = jsonResponse["email"].ToString();
                profile.Role = int.Parse(jsonResponse["role"].ToString());
                profile.Phone = jsonResponse["phone"].ToString();      
            }
            catch (HttpRequestException ex)
            {
                // Xử lý các lỗi yêu cầu HTTP (nếu có)
                string responseContent = "Error: " + ex.Message;
                Console.WriteLine(responseContent);
            }
        }
    }
}
