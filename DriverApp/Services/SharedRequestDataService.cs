﻿using Library.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriverApp.Services
{
    public class SharedRequestDataService
    {
        public BookingRequestItem currentReq { get; set; } = null;

        public SharedRequestDataService(BookingRequestItem req)
        {
            currentReq = req;
        }
    }
}
