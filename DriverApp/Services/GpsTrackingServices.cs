﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Shared;

namespace DriverApp.Services
{
	public class GpsTrackingServices : IAsyncDisposable //-> TODO phần DispoAsync()
	{
		HubConnection _hubConnection;
		private readonly NavigationManager _navigationManager;

		public GpsTrackingServices(NavigationManager navigationManager)
		{
			_navigationManager = navigationManager;
		}

		public async Task StartConnection()
		{
            ServerURL sUrl = new ServerURL();
            if (_hubConnection is not null)
				return;
			_hubConnection = new HubConnectionBuilder()
				.WithUrl(_navigationManager.ToAbsoluteUri(sUrl.sURL + "/gpstrackinghub"))
				.Build();

			await _hubConnection.StartAsync();
			await JoinGroup();
		}

		public async Task JoinGroup()
		{
			if (_hubConnection is null) return;
			await _hubConnection.SendAsync("AddToGroup", "drivers");
		}

		public async Task LeaveGroup()
		{
			if (_hubConnection is null) return;
			await _hubConnection.SendAsync("RemoveFromGroup", "drivers");
		}

		//khi thay đổi vị trí gps thì gọi phương thức này
		public async Task SendGpsToUserAndCallCenter(GpsItem item)
		{
			if (_hubConnection is null) return;
			await _hubConnection.SendAsync("SendGpsToUserAndCallCenter", item);
		}


		// ngắt kết nối khi rời khỏi trang (chuyển trang khác) là không cần thiết
		//TODO : điều này nên được gọi khi tắt chương trình
		public async ValueTask DisposeAsync()
		{
			if (_hubConnection is not null)
			{
				await LeaveGroup();
				await _hubConnection.DisposeAsync();
			}
		}
	}

}
