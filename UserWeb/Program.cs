using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Majorsoft.Blazor.Components.Maps;
using UserLibrary.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();
builder.Services.AddMapExtensions();
builder.Services.AddScoped<GpsTrackingServices>();
builder.Services.AddScoped<BookingServices>();
builder.Services.AddSingleton<ProfileService>(provider => new ProfileService());

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseStaticFiles();

app.UseRouting();

app.MapBlazorHub();
app.MapFallbackToPage("/_Host");

app.Run();
