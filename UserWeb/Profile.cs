﻿public enum ProfileType
{
    User,
    Driver,
    Operator,
    Admin
}

public abstract class Profile
{
    public string ObjectDataId { get; set; } = string.Empty;

    public string Name { get; set; } = string.Empty;

    public string Phone { get; set; } = string.Empty;

    public ProfileType Role { get; set; } = ProfileType.User;
}

