﻿var map;
var currentMarker;  // This will hold the current marker
var fromMarker;  // This will hold the current marker
var currentPath;
var isFrom = true;
var markers = [];
var driverMarker;


function initializeMap(mapId) {
    map = L.map(mapId, {
        zoomControl: false
    }).setView([51.505, -0.09], 13);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '© OpenStreetMap contributors'
    }).addTo(map);


    // Function to handle map clicks
    function onMapClick(event) {

        // Get the clicked coordinates
        var lat = event.latlng.lat;
        var lon = event.latlng.lng;

        // Remove the previous marker if it exists
        if (currentMarker) {
            map.removeLayer(currentMarker);
        }

        // Drop a pin on the clicked location
        currentMarker = L.marker([lat, lon]).addTo(map);

        if (currentMarker && fromMarker) {
            getShortestPath(currentMarker, fromMarker);
        }

        // Reverse geocode the coordinates to get a name
        fetch(`https://nominatim.openstreetmap.org/reverse?format=json&lat=${lat}&lon=${lon}`)
            .then(response => response.json())
            .then(data => {
                document.getElementById('locationField').value = data.display_name;
                console.log(document.getElementById("locationField").value);
            });
    }

    // Get the user's location on map load
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var lat = position.coords.latitude;
            var lon = position.coords.longitude;

            // Set the map view to the user's location
            map.setView([lat, lon], 13);

            // Add a pin to the user's location
            currentMarker = L.marker([lat, lon]).addTo(map);

            fetch(`https://nominatim.openstreetmap.org/reverse?format=json&lat=${lat}&lon=${lon}`)
                .then(response => response.json())
                .then(data => {
                    document.getElementById('locationField').value = data.display_name;
                    console.log(document.getElementById("locationField").value);
                });

            // Log the user's location
            console.log(`User's Location - Lat: ${lat}, Lon: ${lon}`);
        });
    } else {
        console.log("Geolocation is not supported by this browser.");
    }

    // Listen for map click events
    map.on('click', onMapClick);
}

async function fetchLocationSuggestions(query) {
    const response = await fetch(`https://nominatim.openstreetmap.org/search?q=${encodeURIComponent(query)}&format=json&limit=5`);
    const data = await response.json();
    return data.map(item => item.display_name);
}

async function setPinByLocation(query) {
    const response = await fetch(`https://nominatim.openstreetmap.org/search?q=${encodeURIComponent(query)}&format=json&limit=1`);
    const data = await response.json();
    if (data && data.length > 0) {
        const lat = parseFloat(data[0].lat);
        const lon = parseFloat(data[0].lon);

        // Remove the previous marker if it exists
        if (currentMarker) {
            map.removeLayer(currentMarker);
        }

        // Add a pin on the new location
        currentMarker = L.marker([lat, lon]).addTo(map);

        // Optionally, center the map on the new location
        map.setView([lat, lon], 13);
    }
}

async function handleBack(contentType) {
    switch (contentType) {
        case 1: {
            if (currentMarker) {
                map.removeLayer(currentMarker);
            }
            if (currentPath) {
                map.removeLayer(currentPath);
            }
            currentMarker = fromMarker;
            currentMarker.addTo(map);

            fromMarker = null;
            currentPath = null;
            isFrom = false;
            break;
        }
        case 2: {
            break;
        }
    }
}

async function handleNext() {
    fromMarker = currentMarker;
    fromMarker.addTo(map);

    currentMarker = null;
    isFrom = true;
}

function getShortestPath(startMarker, endMarker) {
    const startLat = startMarker.getLatLng().lat;
    const startLon = startMarker.getLatLng().lng;
    const endLat = endMarker.getLatLng().lat;
    const endLon = endMarker.getLatLng().lng;

    fetch(`https://router.project-osrm.org/route/v1/driving/${startLon},${startLat};${endLon},${endLat}?overview=full`)
        .then(response => response.json())
        .then(data => {
            if (data && data.routes && data.routes.length > 0 && data.routes[0].geometry) {
                const coordinates = decodePolyline(data.routes[0].geometry);
                if (currentPath) {
                    map.removeLayer(currentPath);
                }

                currentPath = L.polyline(coordinates, { color: "blue" });

                // Draw the path on the map
                currentPath.addTo(map);
            } else {
                console.error("Unexpected data structure from OSRM:", data);
            }
        })
        .catch(error => {
            console.error("Error fetching route from OSRM:", error);
        });
}


function decodePolyline(encoded) {
    let index = 0, lat = 0, lng = 0, coordinates = [], shift = 0, result = 0, byte = null, latitude_change, longitude_change, factor = Math.pow(10, 5);

    while (index < encoded.length) {
        byte = null;
        shift = 0;
        result = 0;
        do {
            byte = encoded.charCodeAt(index++) - 63;
            result |= (byte & 0x1F) << shift;
            shift += 5;
        } while (byte >= 0x20);

        latitude_change = ((result & 1) ? ~(result >> 1) : (result >> 1));
        shift = result = 0;

        do {
            byte = encoded.charCodeAt(index++) - 63;
            result |= (byte & 0x1F) << shift;
            shift += 5;
        } while (byte >= 0x20);

        longitude_change = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lat += latitude_change;
        lng += longitude_change;
        coordinates.push([lat / factor, lng / factor]);
    }

    return coordinates;
}

function getValueFromInput(inputID) {
    return document.getElementById(inputID).value;
}

function CreateCustomIcon(url) {
    return new L.Icon({
        iconUrl: url,
        iconSize: [32, 32],
        iconAnchor: [16, 32],
        popupAnchor: [0, -32],
    });
}

function placePinsOnMap(pins) {
    markers.forEach(marker => map.removeLayer(marker));
    markers = [];

    let customIcon = CreateCustomIcon('/images/Car.svg');

    pins.forEach(pin => {
        let marker = L.marker([pin.lat, pin.lon], { icon: customIcon }).addTo(map);
        markers.push(marker);
    })
}

function getCurrentLat() {
    return currentMarker.getLatLng().lat;
}

function getCurrentLon() {
    return currentMarker.getLatLng().lng;
}

function getFromLat() {
    return fromMarker.getLatLng().lat;
}

function getFromLon() {
    return fromMarker.getLatLng().lng;
}

function updateDriverMarker(lat, lon) {
    if (driverMarker) {
        map.removeLayer(driverMarker);
    }

    // Drop a pin on the clicked location
    let customIcon = CreateCustomIcon('/images/Car.svg');

    let driverMarker = L.marker([lat, lon], { icon: customIcon }).addTo(map);
}